const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const productRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');
const userRoutes = require('./api/routes/user');

//connect to mongoDB
mongoose.connect("mongodb://jbonino:"+
    process.env.MONGO_ATLAS_PWD+
    "@node-rest-shop-shard-00-00-tzwan.mongodb.net:27017,node-rest-shop-shard-00-01-tzwan.mongodb.net:27017,node-rest-shop-shard-00-02-tzwan.mongodb.net:27017/test?ssl=true&replicaSet=node-rest-shop-shard-0&authSource=admin"
);
mongoose.Promise = global.Promise;

//logger middleware
app.use(morgan('dev'));
//make uploads public
app.use('/uploads', express.static('uploads'));
//makes it easier to read/use
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//format headers -- CORS
app.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin",'*');
    res.header(
        "Access-Control-Allow-Headers",
        'Origin, X-Requested-With,Content-Type,Accept,Authorization'
    );
    //on all write requests, client will ask for options to see if it can post,put
    if(req.method === 'OPTIONS'){
        res.header("Access-Control-Allow-Methods","POST, PUT, PATCH, UPDATE, DELETE, GET");
        return res.status(200).json({});
    }
    next();
});

//takes all /products requests and send to productRoutes
app.use('/products', productRoutes);
//takes all /orders requests and send to orderRoutes
app.use('/orders', orderRoutes);
//takes all /user requests and send to orderRoutes
app.use('/user', userRoutes);

//handles all other requests, Since we made it this far, we were not a product or order.
app.use((req,res,next)=>{
    const error = new Error('Page Not Found');
    error.status = 404;
    next(error);
});
//handles all errors thrown. formats json error
app.use((error,req,res,next)=>{
    res.status(error.status||500);
    res.json({
        error:{
            message: error.message
        }
    });
});

module.exports = app;
