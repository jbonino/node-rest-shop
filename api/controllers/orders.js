const Order = require('../models/order');
const Product = require('../models/product');
const mongoose = require('mongoose');

exports.orders_get_all = (req,res,next) =>{
    Order.find()
        .populate('product', 'name')
        .exec()
        .then(orders=>{
            if(orders.length<1){
                return res.status(404).json({
                    message: 'No orders found!'
                });
            }
            res.status(200).json({
                count: orders.length,
                orders: orders.map(order=>{
                    return{
                        product: order.product,
                        quantity: order.quantity,
                        _id:order.id,
                        request:{
                            type:'GET',
                            url:'http://localhost:3000/orders/'+ order._id
                        }
                    }
                })
            });
        })
        .catch(err=>{
            res.status(500).json({
                error:err
            });
        });
}

exports.orders_post = (req,res,next) =>{
    console.log('order post');
    Product.findById(req.body.productId)
            .exec()
            .then(product=>{
                if(!product){
                    return res.status(404).json({
                        message: 'Product Not Found. Use productId in Json body'
                    });
                }
                const order = new Order({
                    _id: new mongoose.Types.ObjectId,
                    product: req.body.productId,
                    quantity: req.body.quantity
                })
                return order.save();
            })
            .then(doc=>{
                res.status(201).json({
                    message: 'Order Created'
                });
            })
            .catch(err=>{
                res.status(500).json({
                    error:err.message
                });
            });
}

exports.orders_get_id = (req,res,next) =>{
    Order.findById(req.params.orderId)
            .select('name quantity _id')
            .populate('product', 'name price _id')
            .exec()
            .then(order=>{
                if(!order){
                    res.status(404).json({
                        message: 'Order not found!'
                    });
                }
                else{
                    res.status(200).json(order);
                }
            })
            .catch(err=>{
                res.status(500).json({
                    error: err
                });
            });
}

exports.orders_delete_id = (req,res,next) =>{
    Order.remove({_id: req.params.orderId})
            .then(doc=>{
                res.status(200).json({
                    message: 'Removed Successfully'
                });
            })
            .catch(err=>{
                res.status(500).json({
                    error:err
                })
            });
}

exports.orders_update_id = (req,res,next)=>{
    const id = req.params.orderId;
    const updateOps = {};
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value
    }
    Order.update({_id: id},{$set: updateOps})
            .exec()
            .then(doc=>{
                res.status(200).json({
                    message: 'updated successfully'
                });
            })
            .catch(err=>{
                res.status(500).json({
                    error:err
                })
            });
}