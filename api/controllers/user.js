const User = require('../models/user');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.user_signup = (req,res,next)=>{
    User.find({email: req.body.email})
        .exec()
        .then(user=>{
            if(user.length>0){
                return res.status(404).json({
                    message: 'User already exists with email "'+req.body.email+'"'
                });
            }
            else{
                bcrypt.hash(req.body.password,10,(err,hashPass)=>{
                    if(err){
                        return res.status(500).json({
                            error: err.message+". Password field is required"
                        });
                    }
                    else{
                        const newUser = new User({
                            _id: new mongoose.Types.ObjectId,
                            email: req.body.email,
                            password: hashPass
                        });
                        newUser.save()
                            .then(doc=>{
                                res.status(201).json({
                                    message: 'Created new User',
                                    user: newUser
                                });
                            })
                            .catch(err=>{
                                res.status(500).json({
                                    error: err
                                });
                            });
                    }
                })
            }
        })
}

exports.user_login = (req,res,next)=>{
    User.find({email: req.body.email})
        .exec()
        .then(user=>{
            if(user.length < 1){
                return res.status(401).json({
                    message: 'Bad Authorization'
                });
            }
            else{          
                bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                    if(result){
                        const token = jwt.sign(
                            {
                                email: user[0].email,
                                id: user[0]._id
                            },
                            process.env.MONGO_ATLAS_PWD,
                            {
                                expiresIn:'1h'
                            }
                        )
                        return res.status(200).json({
                            message: "auth success",
                            token: token
                        })
                    }
                    res.status(401).json({
                        message: 'Bad Authorization'
                    });
            })}
        })
        .catch(err=>{
            res.status(500).json({
                error: err.message
            })
        })
}

exports.user_delete = (req,res,next)=>{
    User.remove({email:req.body.email})
        .then(doc=>{
            if(doc.length>0){
                res.status(200).json({
                    message: 'succesfully removed user '+req.body.email
                });
            }
            else res.status(500).json({
                message: 'User does not exist'
            })
        })
        .catch(err=>{
            res.status(500).json({
                error: err
            });
        });
}