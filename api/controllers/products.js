const Product = require('../models/product');
const mongoose = require('mongoose');

exports.products_get_all = (req, res, next) => {
    Product.find()
        .exec()
        .then(doc=>{
            const response = {
                count: doc.length,
                product: doc.map(doc=>{
                    return{
                        name: doc.name,
                        price: doc.price,
                        _id:doc.id,
                        productImage:doc.productImage,
                        request:{
                            type:'GET',
                            url:'http://localhost:3000/products/'+ doc.id
                        }
                    }
                })
            };
            console.log(response);
            res.status(200).json(response);
        })
        .catch(err=>{
            console.log(err);
            res.status(500).json({error:err})
        });
}
exports.products_post = (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImage: req.file.path
    });
    product.save()
        .then(result=>{
            res.status(201).json({
                message: 'Added successfully',
                createdProduct: product
        })
        .catch(err=>{
            res.status(500).json({error:err})
        });
    });
}
exports.products_get_id = (req, res, next) => {
    const id = req.params.productId;
    Product.findById(id)
            .select('name price _id productImage')
            .exec()
            .then(doc =>{
                console.log(doc);
                if(doc){
                    res.status(200).json(doc);
                }
                else{
                    res.status(404).json({message:'no entry found'})
                }
            })
            .catch(err=>{
                console.log(err);
                res.status(500).json({error:err})
            });

}
exports.products_update_id = (req, res, next) => {
    const id = req.params.productId;
    //We are expecting a json array of objects with propName and value
    //we then just change exactly what we need to in object
    const updateOps = {};
    for(const ops of req.body){
        updateOps[ops.propName] = ops.value;
    }
    Product.update({_id: id},{$set: updateOps})
            .exec()
            .then(doc=>{
                res.status(200).json({
                    message: 'updated successfully'
                });
            })
            .catch(err=>{
                res.status(500).json({
                    error:err
                })
            });
}
exports.products_delete_id = (req, res, next) => {
    const id = req.params.productId;
    Product.remove({_id: id})
            .exec()
            .then(doc=>{
                res.status(200).json({
                    message: 'Removed Successfully'
                });
            })
            .catch(err=>{
                res.status(500).json({
                    error:err
                })
            });
}