const express = require('express');
const router = express.Router();

const checkAuth = require('../middleware/checkAuth');
const OrderController = require('../controllers/orders');

router.get('/', checkAuth, OrderController.orders_get_all);
router.post('/', checkAuth, OrderController.orders_post);
router.get('/:orderId', checkAuth, OrderController.orders_get_id);
router.delete('/:orderId',checkAuth, OrderController.orders_delete_id);
router.patch('/:orderId',checkAuth, OrderController.orders_update_id);

module.exports = router;
